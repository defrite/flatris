FROM node

WORKDIR /skillbox
COPY package.json /skillbox
RUN yarn install 

COPY . /skillbox

RUN yarn build

CMD yarn start

EXPOSE 3000

